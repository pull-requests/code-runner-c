# Code-runner
Automated CI mechanism developed by Sulin Project.

### Dependencies
 - Python3.x
   - python3-yaml

## Usage: 
code-runner <project path or git adress> <workdir>

## Installation:
run `make install` as root

## Net install:
```shell
wget -O - https://gitlab.com/sulinos/devel/code-runner/-/raw/master/netinstall | bash
```

## Writing build.yaml
Example yaml file:

```yaml
name: example
on-start: echo "$NAME start"
on-error: echo "$NAME error $LOGFILE $EXITCODE"
on-done:  echo "$NAME done"

steps:
  - build
  - test
  - install

jobs:
  build:
    uses: ssh
    variant: user@server:port
    run: echo "Running on server"

  test:
    uses: vagrant
    variant: debian/bullseye64
    run: 
      - echo "Running on vagrant image"
      - echo "Hello world"

  install:
    uses: docker
    variant: debian:latest
    run: |
      echo "Running on docker"
      echo "code-runner stuff"

```

## Connect with gitea
Start webhook/gitea.py with root or create service then start.

## Writing plugin:
create plugin/xxx.py file and write like this:
```python
import util
# If you want to run command. you must use util.run(xxx,quiet=True)
def deploy(variant):
    """deploy commands"""

def call(command):
    """run command in your plugin"""

def cleanup():
    """clean commands"""
```

## Using custom config file:
`CONFIG_FILE=/your/config/file.yaml code-runner ...`

