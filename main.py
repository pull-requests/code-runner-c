#!/usr/bin/python3
import sys
import yaml
import os
import importlib
import pathlib
import util #
# import time

### Disable stdin
if sys.stdin:
    os.close(0)
config_file = "/lib/code-runner/config.yaml"

if "CONFIG_FILE" in os.environ:
    config_file = os.environ["CONFIG_FILE"]
config = yaml.safe_load(open(config_file,"r"))

### Define directory values
archive_dir = util.fixdir(config["directory"]["archive"])
build_dir = util.fixdir(config["directory"]["build"])
log_dir = config["directory"]["log"]
template_dir = util.fixdir(config["directory"]["template"])
target = sys.argv[1]

### Target find
if "://" in target:
    builddir = "{}/{}".format(build_dir,sys.argv[2])
    if os.path.exists(builddir):
        os.system("rm -rf '{}'".format(builddir))
    os.system("git clone '{}' '{}'".format(target,builddir))
    os.system("cd '{}' && git submodule init".format(builddir))
    target = builddir

util.info("Target: {}".format(target))
os.environ["PATH"]="/bin:/sbin:/usr/sbin:/usr/bin"

### Target detection
errorFound = False
template = None

if not os.path.exists(target+"/build.yaml"):
    errorFound = True
    for name in config["template"]:
        if os.path.exists("{}/{}".format(target,config["template"][name])):
            os.environ["TEMPLATE"]=name
            template = name
            errorFound = False
            break

if errorFound:
    util.err("Target is invalid")
    sys.exit(1)

### Go to target and load yaml
target = os.path.realpath(target)
os.chdir(target)

if template:
    data = yaml.safe_load(open("{}/{}.yaml".format(template_dir,template),"r"))

else:
    data = yaml.safe_load(open(target+"/build.yaml","r"))

jobs = data["jobs"]
name = data["name"]

### Reset environments
util.data = data
util.config = config
util.reset_env()

### Define directory after target
artifact_archive = "{}/{}/artifacts/".format(archive_dir, os.path.basename(target))
log_archive = "{}/{}/logs/".format(archive_dir, os.path.basename(target))

### Set new environments
os.environ["TARGET"] = str(target)
os.environ["TEMPLATE"] = str(template)
os.environ["ARCHIVE"] = archive_dir
os.environ["ARTIFACTS"] = artifact_archive
os.environ["LOGS"] = log_archive

### Error handler action definition
util.error_action = util.error_action

### Run steps
util.run_hook("on-start")
errorFound = False

for step in data["steps"]:
    if errorFound:
        util.warn("Action: {} skiped.".format(step))
        continue

    if "skip-actions" in config:
        if step in config["skip-actions"]:
            util.info("Action: {} skiped.".format(step))
            continue

    ### Async check
    is_async = False

    if "async" in jobs[step]:
        is_async = jobs[step]["async"]

    if not is_async or os.fork():
        ### Go target and import module
        os.chdir(target)
        module_name = jobs[step]["uses"]
        os.environ["PLUGIN"]=module_name
        module = importlib.import_module("plugin."+ module_name, package=None)
        ### Write action information and logging init
        util.info("Action started:{}/{}".format(name,step))
        if not os.path.exists(log_dir):
            pathlib.Path(log_dir).mkdir(parents=True)
        util.logfile = os.path.realpath("{}/{}-{}.log".format(log_dir,name,step))

        if os.path.exists(util.logfile):
            os.unlink(util.logfile)
        ### Subdirectory init process

        if "subdir" in jobs[step]:
            subdir = util.safedir(jobs[step]["subdir"])
            if not os.path.exists(subdir):
                pathlib.Path(subdir).mkdir(parents=True)
            util.info("Sub directory:{}".format(subdir))
            os.environ["SUBDIR"]=subdir
            os.chdir(subdir)
        ### Deploy plugin
        variant = ""

        if "variant" in jobs[step]:
            variant = jobs[step]["variant"]

        os.environ["VARIANT"] = variant
        module.deploy(variant)
        ### Call plugin
        os.environ["ACTION"]=step
        util.run_hook("on-start-step")

        if type(jobs[step]["run"]) == type(""):
            if not module.call(jobs[step]["run"]):
                errorFound = True

        else:
            for line in jobs[step]["run"]:

                if not errorFound:

                    if not module.call(line):
                        errorFound = True
        util.run_hook("on-done-step")
        util.info("Action cleanup:{}/{}".format(name,step))
        module.cleanup()
        ### Write action information
        util.info("Action finished:{}/{}".format(name,step))

### Done action
if not errorFound:
    util.run_hook("on-done")
    util.info("DONE")
util.restore_env()

### Archive log if enabled
if config["settings"]["archive-log"]:
    util.info("Archive log files")
    util.fixdir(log_archive)
    util.run("cp -pfr '{}'/'{}'/* '{}' || true".format(
        target,log_dir, log_archive),quiet=True)

### Copy artifacts
if "artifacts" in data:
    util.info("Archive artifacts files")
    if type(data["artifacts"]) == type(""):
        data["artifacts"] = [data["artifacts"]]

    for dir in data["artifacts"]:
        util.fixdir(artifact_archive)
        util.run("cp -pfr '{}'/'{}'/* '{}' || true".format(
            target,dir, artifact_archive),quiet=True)

### Purge build directory if enabled
if "://" in target:
    if config["settings"]["remove-target"]:
        util.info("Removing target")
        util.run("rm -rf '{}' || true".format(target))
