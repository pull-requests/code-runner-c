build:
	: please run make install
install:
	mkdir -p $(DESTDIR)/lib/code-runner
	cp -prfv * $(DESTDIR)/lib/code-runner/
	chmod +x $(DESTDIR)/lib/code-runner/* -R
	ln -s ../../lib/code-runner/main.py $(DESTDIR)/bin/code-runner