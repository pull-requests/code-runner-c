import os
import sys
import subprocess
import time
import pathlib

def warn(msg):
    print("\x1b[33;1m{}\x1b[;0m".format(msg),file=sys.stderr)

def log(msg):
    print("\x1b[32;1m{}\x1b[;0m".format(msg),file=sys.stderr)

def info(msg):
    print("\x1b[34;1m{}\x1b[;0m".format(msg),file=sys.stderr)

def err(msg):
    print("\x1b[31;1m{}\x1b[;0m".format(msg),file=sys.stderr)

logfile="/dev/stdout"
error_action = None
data = None
config = None

def run_args(cmd=None, quiet=False):
    log("Run:{}".format(cmd[-1]))
    try:
        with open(logfile, "a") as output:
            htime = time.asctime(time.localtime(time.time()))
            if not quiet:
                output.write("##### {} #####\n".format(htime))
                output.write(cmd[-1]+"\n")
                output.write("##############\n")
            output.flush()
            if not quiet:
                i = subprocess.call(cmd, stdin=None, stderr=output, stdout=output);
            else:
                i = subprocess.call(cmd, stdin=None, stderr=None, stdout=None);
            output.flush()
            if 0 != i:
                err("Failed to run command: {}\n  => Exit with: {}\n".format(cmd[-1],i))
                output.write("Failed to run command: {}\n  => Exit with: {}\n".format(cmd[-1],i))
                output.flush()
                if error_action:
                    error_action(logfile,i)
                return False
    except:
        err("Failed to run command: {}\n  => Unknown Error\n".format(cmd[-1]))
        if error_action:
            error_action(logfile,1)
            return False
    return True


def run(cmd=None, quiet=False):
    return run_args(["/bin/bash", "-e", "-c", cmd],quiet)

def safedir(path):
    path = path.replace("~","./")
    path = path.replace("..","./")
    path = path.replace(" ","-")
    return path

def fixdir(directory):
    directory = directory.replace("~",os.environ["HOME"])
    directory = os.path.realpath(directory)
    if not os.path.exists(directory):
        pathlib.Path(directory).mkdir(parents=True)
    return directory

env_backup = None
def reset_env():
    global env_backup
    env_backup = os.environ.copy()
    os.environ.clear()
    os.environ["PATH"] = env_backup["PATH"]
    os.environ["HOME"] = env_backup["HOME"]
    os.environ["NAME"] = data["name"]
    os.environ["DEBIAN_FRONTEND"]="noninteractive"
    if "environment" in config:
        for env in config["environment"]:
            os.environ[str(env)] = config["environment"][env]
    if "environment" in data:
        for env in data["environment"]:
            os.environ[str(env)] = data["environment"][env]

def restore_env():
    os.environ.clear()
    os.environ.update(env_backup)

def error_action(logfile,exit_status):
    os.environ["LOGFILE"]=logfile
    os.environ["EXITCODE"]=str(exit_status)
    run_hook("on-error")

def run_hook(hook_name):
    if data and hook_name in data:
        if type(data[hook_name]) == type(""):
            line = data[hook_name]
            os.system(str(line))
        else:
            for line in data[hook_name]:
                os.system(str(line))
    if config and hook_name in config["global-actions"]:
        for line in config["global-actions"][hook_name]:
            os.system(str(line))
