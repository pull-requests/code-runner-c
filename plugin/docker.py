import os
import util, subprocess

dockerid = ""

def deploy(variant):
    global dockerid
    os.environ["DOCKER_HOST"]="unix:///run/user/{}/docker.sock".format(os.getuid())
    util.run("docker pull {}".format(variant),quiet=True)
    curdir=os.getcwd()
    dockerid=subprocess.getoutput("docker run -it -d -v {}:/docker {}".format(curdir,variant))

def call(command):
    os.environ["DOCKER_HOST"]="unix:///run/user/{}/docker.sock".format(os.getuid())
    return util.run_args(["docker","exec", dockerid ,command])
def cleanup():
    os.environ["DOCKER_HOST"]="unix:///run/user/{}/docker.sock".format(os.getuid())
    util.run("docker kill {}".format(dockerid),quiet=True)
    util.run("docker rm {}".format(dockerid),quiet=True)
