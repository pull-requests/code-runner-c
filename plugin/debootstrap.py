import util
import os
dist = "stable"
def deploy(variant):
    global dist
    dist = variant
    if not os.path.exists("./{}-chroot/etc/os-release".format(dist)):
        util.run("debootstrap --no-merged-usr {0} ./{0}-chroot".format(dist),quiet=True)

def call(command):
    for bind in ["dev", "sys", "proc"]:
        util.run("mount --bind /{0} ./{1}-chroot/{0}".format(bind,dist),quiet=True)
    status = util.run_args(["chroot", "./{}-chroot".format(dist), command])
    for bind in ["dev", "sys", "proc"]:
        util.run("umount -lf -R ./{1}-chroot/{0}".format(bind,dist),quiet=True)
    return status

def cleanup():
    util.run("rm -rf ./{}-chroot".format(dist),quiet=True)
